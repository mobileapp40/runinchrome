import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget{


  @override
  Widget build(BuildContext context){
    return MaterialApp(debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text('Weather',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          actions: [
            IconButton(
              onPressed: (){

              },
              icon: Icon(Icons.settings),
            )
          ],
        ),
        body: Container(
          padding: const EdgeInsets.only(top: 100),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.black87,
                Colors.cyanAccent,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                left: 30,
                right: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _cloundIcon(),
                _temprature(),
                _location(),
                _date(),
                _hour(),
                _weekk(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  final _weeks = [ 'วันนี้  22 ํ ----------- 32 ํ' , 'พฤหัส 22 ํ ----------- 31 ํ', 'ศุกร์ 21 ํ ----------- 31 ํ', 'เสาร์ 21 ํ ----------- 31 ํ' , 'อาทิตย์ 24 ํ ----------- 31 ํ', 'จันทร์ 20 ํ ----------- 31 ํ', 'อังคาร 23 ํ ----------- 34 ํ' , 'พุธ 25 ํ ----------- 34 ํ' ];
  _weekk(){
    return Expanded(
      child: Container(
        height: 100,
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: _weeks.length,
            itemBuilder: (context, index) {
              return Container(
                height: 50,
                child: Card(
                  child: Center(
                    child: Text('${_weeks[index]}'),
                  ),
                ),
              );
            }),
      ),
    );
  }
  final times = [ 'ตอนนี้ 22 ํ' , '11:00 26 ํ', '12:00 26 ํ', '13:00 25 ํ' , '14:00 25 ํ', '15:00 24 ํ', '16:00 24 ํ' , '17:00 24 ํ', '18:00 24 ํ' ];
  _hour(){
    return Container(
      height: 100,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Colors.transparent),
        ),
      ),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: times.length,
          itemBuilder: (context, index) {
            return Container(
              width: 50,
              child: Card(
                child: Center(
                  child: Text('${times[index]}'),
                ),
              ),
            );
          }),
    );
  }
  _date() {
    return Row(
      children: [
        Icon(Icons.place, color: Colors.white,),
        SizedBox(
          width: 10,
        ),
        Text(
            'อ.เมืองชลบุรี',
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
            )
        ),
      ],
    );
  }
  _location() {
    return Row(
      children: [
        Text('วันนี้:',
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Text('28.12.2022' ,
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
            )
        ),
      ],
    );
  }
  _temprature(){
    return Text(
      '28 ํ',
      style: TextStyle(
        fontSize: 80,
        fontWeight: FontWeight.w100,
        color: Colors.white,


      ),
      textAlign: TextAlign.center,
    );
  }
  _cloundIcon(){
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Icon(
        Icons.cloud,
        size: 100,
        color: Colors.white,
      ),
    );
  }
}